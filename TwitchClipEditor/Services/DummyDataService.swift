//
//  DummyData.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

import Foundation

struct DummyDataService {
    
    
    /*  •••• Getting the Clip URL to download video content ••••
    
    
     To get any clip video URL, use the clip thumbnail URL and replace the -preview.... by .mp4
     
     Example: https://clips-media-assets2.twitch.tv/UA6yim0f01EMZkHlR2Oivg/AT-cm%7CUA6yim0f01EMZkHlR2Oivg-preview-86x45.jpg -> this is a clip thumbnail URL (JPG)
    
     Replacing "-preview-86x45.jpg" by ".mp4"
     
     https://clips-media-assets2.twitch.tv/UA6yim0f01EMZkHlR2Oivg/AT-cm%7CUA6yim0f01EMZkHlR2Oivg.mp4 leads to the download link for the videoData
    
     
     */
    
    //––––––––––––––––––––––———
    // MARK: - Dummy Data
    //––––––––––––––––––––––———
    
    static let singleClipURL = "https://clips-media-assets2.twitch.tv/UA6yim0f01EMZkHlR2Oivg/AT-cm%7CUA6yim0f01EMZkHlR2Oivg.mp4"
    
    static let dummyClipList: ClipList = ClipList(clips: [
        Clip(id: "CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-", url: "https://clips.twitch.tv/CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-", broadcasterId: "406133309", broadcasterName: "aziz", title: "A test clip", createdAt: "2023-03-28T07:52:38Z", thumbnailUrl: "https://clips-media-assets2.twitch.tv/UA6yim0f01EMZkHlR2Oivg/AT-cm%7CUA6yim0f01EMZkHlR2Oivg-preview-480x272.jpg", duration: 29),
        Clip(id: "CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-", url: "https://clips.twitch.tv/CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-", broadcasterId: "406133309", broadcasterName: "aziz", title: "A test clip", createdAt: "2023-03-28T07:52:38Z", thumbnailUrl: "https://clips-media-assets2.twitch.tv/UA6yim0f01EMZkHlR2Oivg/AT-cm%7CUA6yim0f01EMZkHlR2Oivg-preview-480x272.jpg", duration: 29),
        Clip(id: "CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-", url: "https://clips.twitch.tv/CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-", broadcasterId: "406133309", broadcasterName: "aziz", title: "A test clip", createdAt: "2023-03-28T07:52:38Z", thumbnailUrl: "https://clips-media-assets2.twitch.tv/UA6yim0f01EMZkHlR2Oivg/AT-cm%7CUA6yim0f01EMZkHlR2Oivg-preview-480x272.jpg", duration: 29)
    ])
}

/*
 {
   "id" : "CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-",
   "game_id" : "509658",
   "video_id" : "1777911620",
   "created_at" : "2023-03-28T07:52:38Z",
   "url" : "https:\/\/clips.twitch.tv\/CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-",
   "thumbnail_url" : "https:\/\/clips-media-assets2.twitch.tv\/UA6yim0f01EMZkHlR2Oivg\/AT-cm%7CUA6yim0f01EMZkHlR2Oivg-preview-480x272.jpg",
   "title" : "A test clip",
   "view_count" : 3,
   "language" : "en",
   "broadcaster_name" : "az",
   "creator_id" : "406133309",
   "broadcaster_id" : "406133309",
   "creator_name" : "az",
   "duration" : 29,
   "vod_offset" : 18,
   "embed_url" : "https:\/\/clips.twitch.tv\/embed?clip=CourteousVastSwanPanicVis-bR_TUQxmQeVHY6K-"
 }
 */
