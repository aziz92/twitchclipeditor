import UIKit
import Kingfisher

struct ImageEngine {

    func generateClipImageTemplates(clip: Clip, templates: [Template]) -> [ClipImageTemplate] {
        [
            .init(clip: clip, template: .blackBackground),
            .init(clip: clip, template: .bluredBackground),
            .init(clip: clip, template: .cameraAndArea)
        ]
    }
}
