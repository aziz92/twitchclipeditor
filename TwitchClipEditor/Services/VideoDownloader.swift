//
//  VideoEngine.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

struct VideoDownloader {
    
    func downloadVideo(withURL url: String) -> Clip {
        DummyDataService.dummyClipList.clips.first(where: { $0.url == url }) ?? DummyDataService.dummyClipList.clips[0]
    }
}
