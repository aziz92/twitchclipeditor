import Foundation
import UIKit
import AVFoundation
import CoreImage

class VideoEngine {

    enum OutputType {
        case blackBackground
        case blurryBackground
        case cameraAndArea(cameraRect: CGRect, areaRect: CGRect)

        func backgroundOpacity() -> Float {
            switch self {
            case .blackBackground:
                return 0
            default:
                return 1.0
            }
        }
    }

    static let OutputSize = CGSize(width: 1080, height: 1920)

    private let type: OutputType
    private let sourceURL: URL

    init(type: OutputType, sourceURL: URL) {
        self.type = type
        self.sourceURL = sourceURL
    }

    func export() async -> URL? {
        let composition = AVMutableComposition()

        let firstAsset = AVAsset(url: sourceURL)
        let blurryVideo = await createBlurryVideo(asset: firstAsset)
        let secondAsset = AVAsset(url: blurryVideo)

        guard let firstTrack = try? await firstAsset.loadTracks(withMediaType: .video)[0],
              let secondTrack = try? await secondAsset.loadTracks(withMediaType: .video)[0],
              let audioTrack = try? await firstAsset.loadTracks(withMediaType: .audio)[0],
              let timeRange = try? await firstTrack.load(.timeRange),
              let naturalSize = try? await firstTrack.load(.naturalSize) else {
            return nil
        }

        let scaleRatio = VideoEngine.OutputSize.width/naturalSize.width
        let yOffset = (VideoEngine.OutputSize.height - (naturalSize.height * scaleRatio))/2

        let _ = add(tracks: [firstTrack, secondTrack, audioTrack], composition: composition, timeRange: timeRange)
        let videoComposition = self.videoComposition(with: timeRange, instructions: [
            layerInstruction(for: firstTrack, transform: CGAffineTransform.init(scaleX: scaleRatio, y: scaleRatio).concatenating(CGAffineTransform.init(translationX: 0, y: yOffset))),
            layerInstruction(for: secondTrack, transform: CGAffineTransform.init(scaleX: 1.0, y: 1.0).translatedBy(x: 0, y: 0), opacity: type.backgroundOpacity()),
        ])
        
        let outputURL = getOutputURL(name: "result")
        let exporter = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.videoComposition = videoComposition
        exporter?.outputFileType = .mp4
        exporter?.outputURL = getOutputURL(name: "result")
        await exporter?.export()

        return outputURL
    }

    func addTwitchUrlToVideo(twitchLink: String, originalVideoAspectRatio: Double) async -> URL? {

        let asset = AVAsset(url: sourceURL)
        let composition = AVMutableComposition()

        guard let track = try? await asset.loadTracks(withMediaType: .video)[0],
              let audioTrack = try? await asset.loadTracks(withMediaType: .audio)[0],
              let timeRange = try? await track.load(.timeRange) else {
            return nil
        }

        let _ = add(tracks: [track, audioTrack], composition: composition, timeRange: timeRange)

        let videoComposition = AVMutableVideoComposition()
        videoComposition.instructions = []
        videoComposition.renderSize = VideoEngine.OutputSize
        videoComposition.frameDuration = CMTime(seconds: 1/30, preferredTimescale: 600)

        let textLayer = textLayer(with: twitchLink)
        let textSize = textLayer.preferredFrameSize()
        textLayer.frame = CGRect(origin: CGPoint(x: textSize.height + 5, y: 0), size: textSize)

        let imageLayer = CALayer()
        imageLayer.contents = UIImage(imageLiteralResourceName: "twitch_icon").cgImage
        imageLayer.contentsGravity = .center
        imageLayer.frame = CGRect(x: 0, y: 0, width: textSize.height, height: textSize.height)

        let containerLayer = CALayer()
        containerLayer.addSublayer(imageLayer)
        containerLayer.addSublayer(textLayer)
        let yPosition: CGFloat = (VideoEngine.OutputSize.height / 2) + ((1 / originalVideoAspectRatio) * VideoEngine.OutputSize.width / 2) + 22
        containerLayer.frame = CGRect(x: 12, y: yPosition, width: textSize.width + 5, height: textSize.height)

        addOverlay(layer: containerLayer, to: videoComposition)

        let outputURL = getOutputURL(name: "trimResult")
        let exporter = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.videoComposition = videoComposition
        exporter?.outputFileType = .mp4
        exporter?.outputURL = getOutputURL(name: "trimResult")
        await exporter?.export()
        print(exporter?.status.rawValue)
        print("Export failed with error: \(String(describing: exporter?.error))")
        return outputURL
    }

    func generateThumbnailImages() async -> [UIImage] {
        var frames: [UIImage] = []
        let asset = AVAsset(url: sourceURL)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.maximumSize = CGSize(width: 100, height: 100)
        generator.appliesPreferredTrackTransform = true
        generator.requestedTimeToleranceAfter = .zero
        generator.requestedTimeToleranceBefore = .zero
        generator.appliesPreferredTrackTransform = true

        guard let duration = try? await asset.load(.duration) else { return [] }
        let durationSecs = Int(CMTimeGetSeconds(duration))
        let thumbAvg = durationSecs/6

        for step in 0...5 {
            let time = CMTimeMakeWithSeconds(Float64(thumbAvg * step), preferredTimescale: 30)
            let img = try! generator.copyCGImage(at: time, actualTime: nil)
            frames.append(UIImage(cgImage: img))
        }

        return frames
    }

    func getVideoAspectRatio() async -> Double? {
        let asset = AVAsset(url: sourceURL)
        let videoTracks = try? await asset.loadTracks(withMediaType: .video)

        if let videoTrack = videoTracks?.first, let videoSize = try? await videoTrack.load(.naturalSize) {
            let aspectRatio = Double(videoSize.width / videoSize.height)
            return aspectRatio
        }
        return nil
    }

    //MARK: Internal

    private func add(tracks: [AVAssetTrack], composition: AVMutableComposition, timeRange: CMTimeRange) -> [AVMutableCompositionTrack?]{
        let mutableTracks = tracks.map { track in
            let mutableTrack = composition.addMutableTrack(withMediaType: track.mediaType, preferredTrackID: kCMPersistentTrackID_Invalid)
            try? mutableTrack?.insertTimeRange(CMTimeRange(start: timeRange.start, duration: timeRange.duration), of: track, at: timeRange.start)
            return mutableTrack
        }

        return mutableTracks
    }

    private func layerInstruction(for track: AVAssetTrack, transform: CGAffineTransform, opacity: Float = 1) -> AVMutableVideoCompositionLayerInstruction {
        let layerIns = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        layerIns.setTransform(transform, at: .zero)
        layerIns.setOpacity(opacity, at: .zero)
        return layerIns
    }

    private func videoComposition(with timeRange: CMTimeRange, instructions: [AVMutableVideoCompositionLayerInstruction]) -> AVMutableVideoComposition {
        let ins = AVMutableVideoCompositionInstruction()
        ins.timeRange = CMTimeRange(start: timeRange.start, duration: timeRange.duration)
        ins.layerInstructions = instructions

        let videoComposition = AVMutableVideoComposition()
        videoComposition.instructions = [ins]
        videoComposition.renderSize = VideoEngine.OutputSize
        videoComposition.frameDuration = CMTime(seconds: 1/30, preferredTimescale: 600)

        return videoComposition
    }
    
    private func addOverlay(layer: CALayer, to compositon: AVMutableVideoComposition) {
        let backgroundLayer = CALayer()
        backgroundLayer.backgroundColor = UIColor.red.cgColor
        backgroundLayer.frame = CGRect(origin: .zero, size: VideoEngine.OutputSize)
        
        let videoLayer = CALayer()
        videoLayer.frame = CGRect(origin: .zero, size: VideoEngine.OutputSize)
        
        let outputLayer = CALayer()
        outputLayer.frame = CGRect(origin: .zero, size: VideoEngine.OutputSize)
        outputLayer.addSublayer(backgroundLayer)
        outputLayer.addSublayer(videoLayer)
        outputLayer.addSublayer(layer)
        
        compositon.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: outputLayer)
    }
    
    private func textLayer(with text: String) -> CALayer {
        let layer = CATextLayer()
        layer.string = text
        layer.foregroundColor = UIColor.foreground1?.cgColor
        layer.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        return layer
    }

    private func createBlurryVideo(asset: AVAsset) async -> URL {
        let filter = CIFilter(name: "CIGaussianBlur")
        let compBlur = AVMutableVideoComposition(asset: asset) { request in
            let source: CIImage? = request.sourceImage.clampedToExtent()

            let ratio: CGFloat = 1920/720
            let scale = CGAffineTransform.init(scaleX: ratio, y: ratio)
            let trans = CGAffineTransform.init(translationX: -(1280 * ratio)/2, y: 0)

            let scaled = source?.transformed(by: CGAffineTransformConcat(scale, trans)).cropped(to: CGRect(x: 0, y: 0, width: 1080, height: 1920))

            filter?.setValue(scaled, forKey: kCIInputImageKey)
            filter?.setValue(10.0, forKey: kCIInputRadiusKey)

            let output: CIImage? = filter?.outputImage?.cropped(to: CGRect(x: 0, y: 0, width: 1080, height: 1920))
            if let anOutput = output {
                request.finish(with: anOutput, context: nil)
            }
        }
        compBlur.renderSize = CGSize(width: 1080, height: 1920)


        let outputURL = getOutputURL(name: "temp")
        let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)
        exporter?.videoComposition = compBlur
        exporter?.outputFileType = .mp4
        exporter?.outputURL = outputURL
        await exporter?.export()

        return outputURL
    }

    private func getOutputURL(name: String) -> URL {
        let fileManager = FileManager.default
        let documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        var outputURL = documentsDirectory.appendingPathComponent("output")
        do {
            try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true)
            outputURL = outputURL.appendingPathComponent("\(name).mp4")
        } catch let error {
            print(error)
        }
        try? FileManager.default.removeItem(at: outputURL)
        return outputURL
    }
}

extension VideoEngine.OutputType {

    init(from template: Template) {
        switch template {
        case .blackBackground:
            self = .blackBackground
        case .bluredBackground:
            self = .blurryBackground
        case .cameraAndArea:
            self = .cameraAndArea(cameraRect: .zero, areaRect: .zero)
        }
    }
}
