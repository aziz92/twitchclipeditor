//
//  ViewController.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

import UIKit

class RecentClipsViewController: UIViewController {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //––––––––––––––––––––––———
    // MARK: - Closures
    //––––––––––––––––––––––———
    
    var didSelectClip: ((_ selectedCLip: Clip) -> Void)?
    
    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––
    
    var clipList: ClipList = DummyDataService.dummyClipList {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    //––––––––––––––––––––––———
    // MARK: - View life cycle
    //––––––––––––––––––––––———
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———
    
    let recentClipLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .foreground1
        lbl.text = "Select recent clip"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        lbl.numberOfLines = 1
        lbl.backgroundColor = .clear
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.contentInset.left = 20
        collectionView.contentInset.right = 20
        
        //Cells
        collectionView.register(ClipCell.self, forCellWithReuseIdentifier: ClipCell.reuseIdentifier)
        
        return collectionView
    }()
    
    func setupUI() {
        view.addSubview(recentClipLbl)
        view.addSubview(collectionView)
        
        recentClipLbl.anchor(top: view.topAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 20, paddingRight: 0)
        collectionView.anchor(top: recentClipLbl.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 14, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, height: 113)
    }
}


extension RecentClipsViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedClip = clipList.clips[indexPath.item]
        self.didSelectClip?(selectedClip)
    }
}
extension RecentClipsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clipList.clips.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ClipCell.reuseIdentifier, for: indexPath) as! ClipCell
        
        let clip = clipList.clips[indexPath.item]
        cell.setupWith(clipItem: clip)
        
        return cell
    }
}

extension RecentClipsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 113)
    }
}
