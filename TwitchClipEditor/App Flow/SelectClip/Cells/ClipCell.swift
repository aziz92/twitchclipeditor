//
//  ClipCell.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

import Foundation
import UIKit
import Kingfisher

class ClipCell: BaseCell {
    
    // ———————————————————
    // MARK: Initializers
    // ———————————————————
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // ———————————————————
    // MARK: - Setup UI
    // ———————————————————

    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true
        return imageView
    }()

    func setupUI() {
        
        isBounceable = true
        layer.cornerRadius = 12
        
        addSubview(imageView)
        imageView.fillSuperview()
        
    }
    
    func setupWith(clipItem: Clip) {
        let url = URL(string: clipItem.thumbnailUrl)
        imageView.kf.setImage(with: url)
    }
    
    override func prepareForReuse() {}
}
