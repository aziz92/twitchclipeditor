//
//  SelectClipViewController.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

import Foundation
import UIKit

class SelectClipViewController: UIViewController {
    
    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––
    
    typealias Factory = ViewControllerFactory & VideoDownloaderFactory
    
    private let factory: Factory
    private lazy var videoDownloader = factory.makeVideoDownloader()
    
    init(factory: Factory) {
        self.factory = factory
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––
    
    private lazy var recentClipViewController = factory.makeRecentClipsViewController()
    private var isDownloadingActive: Bool = false {
        didSet {
            downloadingOverlayView.isHidden = !isDownloadingActive
            downloadingOverlayOverNavagitionBarView.isHidden = !isDownloadingActive
            if isDownloadingActive {
                downloadFromURLTextField.searchTextField.resignFirstResponder()
            }
        }
    }
    
    //––––––––––––––––––––––———
    // MARK: - View life cycle
    //––––––––––––––––––––––———
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupNavigationBar()
        removeTemporaryCreatedFiles()
    }
    
    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———
    
    let scrollView = UIScrollView()
    
    let subtitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .foreground1
        lbl.text = "Sharing your clips & highlights on social can help drive viewers and grow your stream."
        lbl.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var recentClipsContainerView: UIView = {
        let v = UIView()
        return v
    }()
    
    let pasteUrlLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .foreground1
        lbl.text = "or paste a clip url"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        lbl.numberOfLines = 1
        lbl.backgroundColor = .clear
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    lazy var downloadFromURLTextField: ValidationSearchTextField = {
        let searchTextField = ValidationSearchTextField(frame: .zero, isValid: false, validatorCriteria: TwitchURLLinkValidator(), onSearchButtonTapped: selectVideo(withURL:))
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        return searchTextField
    }()

    lazy var downloadingOverlayView: DownloadingOverlayView = {
        let overlayView = DownloadingOverlayView(frame: .zero)
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.insetsLayoutMarginsFromSafeArea = false
        return overlayView
    }()

    lazy var downloadingOverlayOverNavagitionBarView: UIView = {
        let overlayView = UIView(frame: .zero)
        overlayView.backgroundColor = .overlay1
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.insetsLayoutMarginsFromSafeArea = false
        return overlayView
    }()
    
    func setupUI() {
        
        view.backgroundColor = .background1
        
        view.addSubview(scrollView)
        scrollView.fillSuperview()
        scrollView.addSubview(subtitleLbl)
        scrollView.addSubview(recentClipsContainerView)
        scrollView.addSubview(pasteUrlLbl)
        scrollView.addSubview(downloadFromURLTextField)
        subtitleLbl.anchor(top: safeTopAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 4, paddingLeft: 20, paddingBottom: 0, paddingRight: 20)
        recentClipsContainerView.anchor(top: subtitleLbl.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 14, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        pasteUrlLbl.anchor(top: recentClipsContainerView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 20)
        downloadFromURLTextField.anchor(top: pasteUrlLbl.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 16, paddingLeft: 20, paddingRight: 20, height: 60)

        setupDownloadingOverlayView()
        setupRecentClipVC()
    }

    private func setupDownloadingOverlayView() {

        view.addSubview(downloadingOverlayView)
        downloadingOverlayView.fillSuperview()

        navigationController?.navigationBar.addSubview(downloadingOverlayOverNavagitionBarView)
        downloadingOverlayOverNavagitionBarView.anchor(top: navigationController?.navigationBar.topAnchor, left: navigationController?.navigationBar.leftAnchor, bottom: navigationController?.navigationBar.bottomAnchor, right: navigationController?.navigationBar.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)

        isDownloadingActive = false
    }
    
    private func setupRecentClipVC() {
        add(recentClipViewController, view: recentClipsContainerView)
        recentClipViewController.view.fillSuperview()
        recentClipViewController.didSelectClip = { [weak self] selectedClip in
            self?.selectVideo(withURL: selectedClip.url)
        }
    }

    private func setupNavigationBar() {
        self.title = "Promote your clips"
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.backgroundColor = .background1
        navigationController?.navigationBar.tintColor = .foreground1
        let attrs: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.foreground1 as Any
        ]
        navigationController?.navigationBar.largeTitleTextAttributes = attrs
        navigationController?.navigationBar.titleTextAttributes = attrs
    }

    private func selectVideo(withURL url: String) {
        isDownloadingActive = true
        downloadingPercentageUpdating()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            guard let self else { return }
            let clip = self.videoDownloader.downloadVideo(withURL: url)
            self.isDownloadingActive = false

            let selectTemplateVC = self.factory.makeSelectTemplateViewController(for: clip)
            self.navigationController?.pushViewController(selectTemplateVC, animated: true)
        }
    }

    private func downloadingPercentageUpdating() {
        downloadingOverlayView.downloadingPercentage = 0
        Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true) { [weak self] timer in
            guard let self else { return }
            if self.downloadingOverlayView.downloadingPercentage < 100 {
                self.downloadingOverlayView.downloadingPercentage += 1
            } else {
                timer.invalidate()
            }
        }
    }

    private func removeTemporaryCreatedFiles() {
        let fileManager = FileManager.default
        do {
            let documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
            let outputURL = documentsDirectory.appendingPathComponent("output")
            let fileURLs = try fileManager.contentsOfDirectory(at: outputURL, includingPropertiesForKeys: nil)

            for fileURL in fileURLs {
                try fileManager.removeItem(at: fileURL)
            }
        } catch {
            print("Failed to remove files inside folder: \(error.localizedDescription)")
        }
    }
}
