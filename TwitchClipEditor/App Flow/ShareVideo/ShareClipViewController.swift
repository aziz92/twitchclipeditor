import AVFoundation
import UIKit
import Photos

class ShareClipViewController: UIViewController {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    init(clipURL: URL) {
        self.clipURL = clipURL
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––
    
    var clipURL: URL

    //––––––––––––––––––––––———
    // MARK: - View life cycle
    //––––––––––––––––––––––———

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        player.seek(to: .zero)
        player.play()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player.pause()
    }

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    lazy var player: AVPlayer = {
        let player = AVPlayer(url: clipURL)
        return player
    }()

    lazy var videoPlayerView: VideoPlayerView = {
        let videoPlayerView = VideoPlayerView(frame: .zero)
        videoPlayerView.player = player
        videoPlayerView.layer.masksToBounds = true
        videoPlayerView.layer.cornerRadius = 10
        return videoPlayerView
    }()

    lazy var shareButton: PrimaryButton = {
        let shareButton = PrimaryButton(frame: .zero, title: "Share", action: shareButtonTapped)
        return shareButton
    }()

    lazy var saveButton: PrimaryButton = {
        let saveButton = PrimaryButton(frame: .zero, title: "Save", action: saveButtonTapped)
        return saveButton
    }()

    lazy var bottomButtonsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [shareButton, saveButton])
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = 20
        return stackView
    }()

    func setupUI() {
        setupNavigationBar()
        view.backgroundColor = .background1
        view.addSubview(videoPlayerView)
        view.addSubview(bottomButtonsStack)
        videoPlayerView.anchor(top: safeTopAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 30, paddingLeft: 30, paddingRight: 30, height: (view.frame.width - 60) * 1920 / 1080)
        bottomButtonsStack.anchor(left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingLeft: 30, paddingBottom: 30, paddingRight: 30, height: 48)
        view.layoutIfNeeded()
    }

    private func setupNavigationBar() {
        navigationItem.largeTitleDisplayMode = .never
        title = "Share"
        navigationController?.navigationBar.backgroundColor = .clear
    }

    private func shareButtonTapped() {
        let shareActivityViewController = UIActivityViewController(activityItems: [clipURL], applicationActivities: nil)
        navigationController?.present(shareActivityViewController, animated: true)
    }

    private func saveButtonTapped() {
        PHPhotoLibrary.requestAuthorization { [weak self] status in
            guard let self else { return }
            guard status == .authorized else { return }
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.clipURL)
            }) { success, error in
                if success {
                    self.view.overlayMiniAlertView(text: "Saved successfully", width: 200, height: 50, timeDisplayed: 1)
                } else if let error = error {
                    print(error.localizedDescription)
                }
            }
        }
    }
}
