import UIKit
import Kingfisher

class ClipImageTemplateCell: BaseCell {

    // ———————————————————
    // MARK: Initializers
    // ———————————————————

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // ———————————————————
    // MARK: - Setup UI
    // ———————————————————

    private lazy var templateView: UIView = {
        let templateView = UIView()
        templateView.layer.cornerRadius = 8
        templateView.layer.masksToBounds = true
        return templateView
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        let twitchWatermarkView = TwitchUserWatermarkView(frame: .zero, watermarkText: "twitch.tv/username")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var twitchWatermarkView: TwitchUserWatermarkView = {
        return TwitchUserWatermarkView(frame: .zero, watermarkText: "twitch.tv/username")
    }()

    private lazy var proLabelView: UIView = {
        let view = UIView()
        let proLabel = UILabel(frame: .zero)
        proLabel.text = "PRO"
        proLabel.textColor = .black
        proLabel.font = .systemFont(ofSize: 13, weight: .heavy)
        view.backgroundColor = .yellowBrand
        view.addSubview(proLabel)
        proLabel.anchorCenterYToSuperview()
        proLabel.anchor(right: view.rightAnchor, paddingRight: 8)
        return view
    }()

    private func setupUI() {
        isBounceable = true
        layer.cornerRadius = 8

        addSubview(templateView)
        templateView.fillSuperview()

    }

    func setupWith(clipImageTemplate: ClipImageTemplate) {
        switch clipImageTemplate.template {
        case .blackBackground:
            setupBlackBackgroundTemplate(forClip: clipImageTemplate.clip)
        case .bluredBackground:
            guard let imageURL = URL(string: clipImageTemplate.clip.thumbnailUrl) else { return }
            let resource = ImageResource(downloadURL: imageURL)
            KingfisherManager.shared.retrieveImage(with: resource){ [weak self] result in
                switch result {
                case let .success(kfImage):
                    self?.setupBluredBackgroundTemplate(withImage: kfImage.image)
                case let .failure(error):
                    print(error.localizedDescription)
                }
            }
        case .cameraAndArea:
            guard let imageURL = URL(string: clipImageTemplate.clip.thumbnailUrl) else { return }
            let resource = ImageResource(downloadURL: imageURL)
            KingfisherManager.shared.retrieveImage(with: resource){ [weak self] result in
                switch result {
                case let .success(kfImage):
                    self?.setupCameraAndAreaTemplate(withImage: kfImage.image)
                case let .failure(error):
                    print(error.localizedDescription)
                }
            }

        }
    }

    private func setupBlackBackgroundTemplate(forClip clip: Clip) {
        let imageURL = URL(string: clip.thumbnailUrl)
        imageView.kf.setImage(with: imageURL)

        templateView.backgroundColor = .black
        templateView.addSubview(imageView)
        imageView.anchor(left: templateView.leftAnchor, right: templateView.rightAnchor, paddingLeft: 0, paddingRight: 0, height: 95)
        imageView.anchorCenterYToSuperview()

        templateView.addSubview(twitchWatermarkView)
        twitchWatermarkView.anchor(top: imageView.bottomAnchor, left: leftAnchor, right: rightAnchor, paddingTop: 5, paddingLeft: 8, paddingRight: 8)
    }

    private func setupBluredBackgroundTemplate(withImage image: UIImage) {
        imageView.image = image
        let bluredBackgroundImageView = UIImageView(frame: .init(x: 0, y: 0, width: 168, height: 290))
        bluredBackgroundImageView.image = image
        bluredBackgroundImageView.addBlurEffect(style: image.isDark() ? .dark : .light, alpha: 0.75)
        templateView.addSubview(bluredBackgroundImageView)
        bluredBackgroundImageView.fillSuperview()
        templateView.addSubview(imageView)
        imageView.anchor(left: templateView.leftAnchor, right: templateView.rightAnchor, paddingLeft: 0, paddingRight: 0, height: 95)
        imageView.anchorCenterYToSuperview()

        templateView.addSubview(twitchWatermarkView)
        twitchWatermarkView.anchor(top: imageView.bottomAnchor, left: leftAnchor, right: rightAnchor, paddingTop: 5, paddingLeft: 8, paddingRight: 8)
    }

    private func setupCameraAndAreaTemplate(withImage image: UIImage) {
        imageView.image = image
        imageView.contentMode = .scaleAspectFill
        templateView.addSubview(imageView)
        imageView.fillSuperview()
        templateView.addSubview(twitchWatermarkView)
        twitchWatermarkView.anchor(top: topAnchor, paddingTop: 12) // below web cam view (not below top of the super view)
        twitchWatermarkView.anchorCenterXToSuperview()

        templateView.addSubview(proLabelView)
        proLabelView.anchor(bottom: templateView.bottomAnchor, right: templateView.rightAnchor, paddingBottom: 24, paddingRight: 0, width: 50, height: 22)
        proLabelView.roundCorners(radius: 10)
    }
}

