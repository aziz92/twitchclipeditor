import UIKit

final class ClipImageTemplateDataSource: NSObject, UICollectionViewDataSource {

    var clipImageTemplates: [ClipImageTemplate] = []

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        clipImageTemplates.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ClipImageTemplateCell.reuseIdentifier, for: indexPath) as! ClipImageTemplateCell

        let clipImageTemplate = clipImageTemplates[indexPath.row]

        cell.setupWith(clipImageTemplate: clipImageTemplate)
        
        return cell
    }
}
