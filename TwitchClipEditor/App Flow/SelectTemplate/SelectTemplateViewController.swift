import UIKit

class SelectTemplateViewController: UIViewController {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    typealias Factory = ViewControllerFactory & VideoEngineFactory

    private let factory: Factory

    init(factory: Factory, clip: Clip) {
        self.factory = factory
        self.clip = clip
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Constants
    //––––––––––––––––––––––

    private let clip: Clip

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––

    private lazy var templateCollectionVC = factory.makeTemplateCollectionViewController(for: clip, templates: [.bluredBackground, .blackBackground])

    //––––––––––––––––––––––———
    // MARK: - View life cycle
    //––––––––––––––––––––––———

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.navigationBar.backgroundColor = .background1
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.navigationBar.backgroundColor = .clear
    }

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    lazy var subtitleLbl: UILabel = {
        let subtitleLbl = UILabel()
        subtitleLbl.textColor = .foreground1
        subtitleLbl.text = "Let's convert this clip to a social friendly format."
        subtitleLbl.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        subtitleLbl.numberOfLines = 0
        subtitleLbl.translatesAutoresizingMaskIntoConstraints = false
        return subtitleLbl
    }()

    lazy var templatesContainerView: UIView = {
        let view = UIView()
        return view
    }()

    var loadingView: LoadingView = {
        let loadingView = LoadingView(frame: .zero)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.insetsLayoutMarginsFromSafeArea = false
        loadingView.isHidden = true
        return loadingView
    }()

    var loadingOverlayOverNavagitionBarView: UIView = {
        let overlayView = UIView(frame: .zero)
        overlayView.backgroundColor = .overlay1
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.insetsLayoutMarginsFromSafeArea = false
        overlayView.isHidden = true
        return overlayView
    }()

    private func setupUI() {
        title = "Select a template"
        view.backgroundColor = .background1

        view.addSubview(subtitleLbl)
        view.addSubview(templatesContainerView)
        view.addSubview(loadingView)

        subtitleLbl.anchor(top: safeTopAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 4, paddingLeft: 20, paddingRight: 20)
        templatesContainerView.anchor(top: subtitleLbl.bottomAnchor, left: view.leftAnchor, bottom: safeBottomAnchor, right: view.rightAnchor, paddingTop: 32, paddingLeft: 20, paddingBottom: 8, paddingRight: 20)

        loadingView.fillSuperview()
        navigationController?.navigationBar.addSubview(loadingOverlayOverNavagitionBarView)
        loadingOverlayOverNavagitionBarView.anchor(top: navigationController?.navigationBar.topAnchor, left: navigationController?.navigationBar.leftAnchor, bottom: navigationController?.navigationBar.bottomAnchor, right: navigationController?.navigationBar.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)

        setupTemplateCollectionVC()
    }

    private func setupTemplateCollectionVC() {
        add(templateCollectionVC, view: templatesContainerView)
        templateCollectionVC.view.fillSuperview()
        templateCollectionVC.didSelectTemplate = selectTemplate
    }

    private func selectTemplate(_ clipTemplate: ClipImageTemplate) {
        self.showLoadingOverlay()
        guard let clipURL = Bundle.main.url(forResource: "twitch_clip", withExtension: "mp4") else {
            hideLoadingOverlay()
            return
        }
        let engine = self.factory.makeVideoEngine(type: .init(from: clipTemplate.template), sourceURL: clipURL)

        switch clipTemplate.template {
        case .cameraAndArea:
            let selectClipAreaVC = factory.makeSelectClipAreasViewController(for: clipTemplate.clip)
            hideLoadingOverlay()
            navigationController?.pushViewController(selectClipAreaVC, animated: true)
        default:
            Task {
                if let url = await engine.export() {
                    let generatedImages = await engine.generateThumbnailImages()
                    let originalVideoAspectRatio = await engine.getVideoAspectRatio()
                    let trimClipViewController = factory.makeTrimClipViewController(clipURL: url, frames: generatedImages, originalVideoAspectRatio: originalVideoAspectRatio, twitchBroadcasterName: clip.broadcasterName)
                    hideLoadingOverlay()
                    navigationController?.pushViewController(trimClipViewController, animated: true)
                } else {
                    self.hideLoadingOverlay()
                }
            }
        }
    }

    private func showLoadingOverlay() {
        loadingView.isHidden = false
        loadingOverlayOverNavagitionBarView.isHidden = false
        view.isUserInteractionEnabled = false
    }

    private func hideLoadingOverlay() {
        loadingView.isHidden = true
        loadingOverlayOverNavagitionBarView.isHidden = true
        view.isUserInteractionEnabled = true
    }
}
