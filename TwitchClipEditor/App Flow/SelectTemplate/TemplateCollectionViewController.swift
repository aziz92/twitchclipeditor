import UIKit

class TemplateCollectionViewController: UIViewController {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    typealias Factory = ImageEngineFactory

    private let factory: Factory
    private lazy var imageEngine = factory.makeImageEngine()

    init(factory: Factory, clip: Clip, templates: [Template]) {
        self.factory = factory
        self.clip = clip
        self.templates = templates
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Constants
    //––––––––––––––––––––––

    let clip: Clip
    let templates: [Template]

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––

    lazy var clipImageTemplatesDataSource: ClipImageTemplateDataSource = {
        let datasource = ClipImageTemplateDataSource()
        datasource.clipImageTemplates = imageEngine.generateClipImageTemplates(clip: clip, templates: templates)
        return datasource
    }()

    var didSelectTemplate: ((ClipImageTemplate) -> Void)?

    //––––––––––––––––––––––———
    // MARK: - View life cycle
    //––––––––––––––––––––––———

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    lazy var templatesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.alwaysBounceHorizontal = false
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.dataSource = clipImageTemplatesDataSource
        collectionView.delegate = self

        collectionView.register(ClipImageTemplateCell.self, forCellWithReuseIdentifier: ClipImageTemplateCell.reuseIdentifier)
        
        return collectionView
    }()


    func setupUI() {
        view.addSubview(templatesCollectionView)
        templatesCollectionView.fillSuperview()
    }
}

extension TemplateCollectionViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let clipTemplate = clipImageTemplatesDataSource.clipImageTemplates[indexPath.row]
        didSelectTemplate?(clipTemplate)
    }
}

extension TemplateCollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 168, height: 290)
    }
}
