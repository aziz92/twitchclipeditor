import SwiftUI
import AVKit
import MobileCoreServices
import CoreMedia
import AssetsLibrary
import Photos

struct TrimClipView: View {

    let clipURL: URL
    let frames: [UIImage]
    let originalVideoAspectRatio: Double?
    let exportVideoAction: (AVAsset, CMTimeRange, String) -> Void

    let asset: AVAsset
    let player: AVPlayer
    @State var twitchBroadcasterUrl: String
    @State private var offsetLeft: CGFloat = 0
    @State private var offsetRight: CGFloat = 0
    @State private var width: CGFloat = 0
    @State private var playing = true

    var body: some View {
        ZStack(alignment: .bottom) {
            TwitchVideoView(player: player, linkText: $twitchBroadcasterUrl, videoAspectRatio: originalVideoAspectRatio)
                .edgesIgnoringSafeArea(.all)
                .background(Color.black)
            VStack(spacing: 15) {
                TrimClipSliderView(frames: frames, asset: asset, player: player, offsetLeft: $offsetLeft, offsetRight: $offsetRight, width: $width, playing: $playing)
                Button(action: prepareForExport) {
                    Text("Prepare for export")
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(.white)
                        .frame(height: 50)
                        .frame(maxWidth: .infinity)
                        .background(Color.buttonBackround)
                        .cornerRadius(15)
                        .padding(.horizontal, 12)
                }
            }
            .padding(.horizontal, 20)
            .padding(.bottom, 34)
        }
        .toolbarBackground(.clear)
    }

    // MARK: - Actions

    private func prepareForExport() {
        Task {
            do {
                let duration = try await asset.load(.duration)
                let durationSeconds = CMTimeGetSeconds(duration)
                let startTime = CMTime(seconds: Double(offsetLeft / width * durationSeconds), preferredTimescale: 1000)
                let endTime = CMTime(seconds: Double((width - offsetRight) / width * durationSeconds), preferredTimescale: 1000)
                let timeRange = CMTimeRange(start: startTime, end: endTime)
                exportVideoAction(asset, timeRange, twitchBroadcasterUrl)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
