import UIKit
import AVFoundation
import SwiftUI

class TrimClipViewController: UIViewController {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    typealias Factory = ViewControllerFactory

    private let factory: Factory

    init(factory: Factory, clipURL: URL, frames: [UIImage], originalVideoAspectRatio: Double?, twitchBroadcasterName: String) {
        self.factory = factory
        self.originalVideoAspectRatio = originalVideoAspectRatio ?? 1
        self.clipURL = clipURL
        super.init(nibName: nil, bundle: nil)
        let trimClipView = TrimClipView(clipURL: clipURL, frames: frames, originalVideoAspectRatio: originalVideoAspectRatio, exportVideoAction: exportVideoAction, asset: AVAsset(url: clipURL), player: AVPlayer(url: clipURL), twitchBroadcasterUrl: "twitch.tv/\(twitchBroadcasterName)")
        self.trimClipView = UIHostingController(rootView: trimClipView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––

    private var trimClipView: UIHostingController<TrimClipView>?
    let originalVideoAspectRatio: Double
    let clipURL: URL

    //––––––––––––––––––––––———
    // MARK: - View life cycle
    //––––––––––––––––––––––———

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationItem.largeTitleDisplayMode = .never
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationItem.largeTitleDisplayMode = .always
    }


    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    private func setupUI() {
        title = "Trim Clip"
        if let trimClipView {
            view.addSubview(trimClipView.view)
            trimClipView.view.fillSuperview()
        }
    }

    //––––––––––––––––––––––———
    // MARK: - Actions
    //––––––––––––––––––––––———

    private func exportVideoAction(videoAsset: AVAsset, timeRange: CMTimeRange, twitchUsernameLink: String) {
        cropVideo(asset: videoAsset, timeRange: timeRange) { [weak self] result in
            guard let self else { return }
            switch result {
            case let .success(url):
                Task {
                    guard let videoWithTwitchWatermark = await VideoEngine(type: .blackBackground, sourceURL: url).addTwitchUrlToVideo(twitchLink: twitchUsernameLink, originalVideoAspectRatio: self.originalVideoAspectRatio) else { return }
                    let shareViewController = self.factory.makeShareClipViewController(clipURL: videoWithTwitchWatermark)
                    self.navigationController?.pushViewController(shareViewController, animated: true)
                }
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }

    //––––––––––––––––––––––———
    // MARK: - Methods
    //––––––––––––––––––––––———

    private func cropVideo(asset: AVAsset, timeRange: CMTimeRange, completionHandler: @escaping ((Result<URL, Error>) -> Void))  {
        let manager = FileManager.default
        guard let documentDirectory = try? manager.url(for: .documentDirectory,
                                                       in: .userDomainMask,
                                                       appropriateFor: nil,
                                                       create: true) else {return}
        var outputURL = documentDirectory.appendingPathComponent("output")
        do {
            try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("export.mp4")
        } catch let error {
            print(error)
        }

        //Remove existing file
        _ = try? manager.removeItem(at: outputURL)

        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.timeRange = timeRange

        exportSession.exportAsynchronously{
            switch exportSession.status {
            case .completed:
                completionHandler(.success(outputURL))
            case .failed:
                print("failed \(String(describing: exportSession.error))")
            case .cancelled:
                print("cancelled \(String(describing: exportSession.error))")

            default: break
            }
        }
    }
}
