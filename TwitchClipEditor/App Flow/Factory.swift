//
//  Factory.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

import AVFoundation
import Foundation
import UIKit
import SwiftUI

//––––––––––––––––––––––———
// MARK: - Protocols
//––––––––––––––––––––––———

protocol ViewControllerFactory {

    func makeSelectClipViewController() -> SelectClipViewController
    func makeRecentClipsViewController() -> RecentClipsViewController
    func makeSelectTemplateViewController(for: Clip) -> SelectTemplateViewController
    func makeTemplateCollectionViewController(for clip: Clip, templates: [Template]) -> TemplateCollectionViewController
    func makeSelectClipAreasViewController(for clip: Clip) -> SelectClipAreasViewController
    func makeTrimClipViewController(clipURL: URL, frames: [UIImage], originalVideoAspectRatio: Double?, twitchBroadcasterName: String) -> TrimClipViewController
    func makeShareClipViewController(clipURL: URL) -> ShareClipViewController
}

protocol VideoDownloaderFactory {
    
    func makeVideoDownloader() -> VideoDownloader
}

protocol VideoEngineFactory {

    func makeVideoEngine(type: VideoEngine.OutputType, sourceURL: URL) -> VideoEngine
}

protocol ImageEngineFactory {

    func makeImageEngine() -> ImageEngine
}

//––––––––––––––––––––––———
// MARK: - Factory
//––––––––––––––––––––––———

class Factory {
    
    let videoDownloader = VideoDownloader()
    let imageEngine = ImageEngine()
}

//––––––––––––––––––––––———
// MARK: - Extensions
//––––––––––––––––––––––———

extension Factory: ViewControllerFactory {
    
    func makeSelectClipViewController() -> SelectClipViewController {
        return SelectClipViewController(factory: self)
    }
    
    func makeRecentClipsViewController() -> RecentClipsViewController {
        return RecentClipsViewController()
    }

    func makeSelectTemplateViewController(for clip: Clip) -> SelectTemplateViewController {
        return SelectTemplateViewController(factory: self, clip: clip)
    }

    func makeTemplateCollectionViewController(for clip: Clip, templates: [Template]) -> TemplateCollectionViewController {
        return TemplateCollectionViewController(factory: self, clip: clip, templates: templates)
    }

    func makeSelectClipAreasViewController(for clip: Clip) -> SelectClipAreasViewController {
        return SelectClipAreasViewController(factory: self, clip: clip)
    }

    func makeTrimClipViewController(clipURL: URL, frames: [UIImage], originalVideoAspectRatio: Double?, twitchBroadcasterName: String) -> TrimClipViewController {
        return TrimClipViewController(factory: self, clipURL: clipURL, frames: frames, originalVideoAspectRatio: originalVideoAspectRatio, twitchBroadcasterName: twitchBroadcasterName)
    }

    func makeShareClipViewController(clipURL: URL) -> ShareClipViewController {
        return ShareClipViewController(clipURL: clipURL)
    }
}

extension Factory: VideoDownloaderFactory {
    func makeVideoDownloader() -> VideoDownloader { videoDownloader }
}

extension Factory: VideoEngineFactory {
    func makeVideoEngine(type: VideoEngine.OutputType, sourceURL: URL) -> VideoEngine {
        VideoEngine(type: type, sourceURL: sourceURL)
    }
}

extension Factory: ImageEngineFactory {
    func makeImageEngine() -> ImageEngine { imageEngine }
}
