import UIKit
import SwiftUI
import AVFoundation

class SelectClipAreasViewController: UIViewController {

    enum ScreenState {
        case webcam
        case content
    }

    enum WebcamShape: Equatable {
        case square
        case rectangle
        case circle
    }

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    typealias Factory = ViewControllerFactory

    private let factory: Factory

    init(factory: Factory, clip: Clip) {
        self.factory = factory
        self.clip = clip
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Constants
    //––––––––––––––––––––––

    private let clip: Clip
    private var screenState: ScreenState = .webcam {
        didSet {
            switch screenState {
            case .webcam:
                contentSelectAreaButton.isSelectedState = false
                shapeSizeSlider.value = webcamShapeSliderValue
                contentShapeSelectionView.view.removeFromSuperview()
                selectShapeContainter.addSubview(webcamShapeSelectionView.view)
                webcamShapeSelectionView.view.fillSuperview()
            case .content:
                webcamSelectAreaButton.isSelectedState = false
                shapeSizeSlider.value = contentShapeSliderValue
                webcamShapeSelectionView.view.removeFromSuperview()
                selectShapeContainter.addSubview(contentShapeSelectionView.view)
                contentShapeSelectionView.view.fillSuperview()
            }
        }
    }
    private var selectedWebcamShape: WebcamShape = .square
    private var webcamShapeSliderValue: Float = 0.5
    private var contentShapeSliderValue: Float = 1.0 {
        didSet {
            let newWidth = (125 / 2) + (contentShapeSliderValue * 125 / 2)
            let newHeight = (223 / 2) + (contentShapeSliderValue * 223 / 2)
            contentShapeSize = .init(width: CGFloat(newWidth), height: CGFloat(newHeight))
        }
    }
    private var contentShapeSize = CGSize(width: 125, height: 223) {
        didSet {
            contentSelectionDraggableView.frame = .init(x: contentShapeLocation.x, y: contentShapeLocation.y, width: contentShapeSize.width, height: contentShapeSize.height)
        }
    }
    private var contentShapeLocation: CGPoint!

    //––––––––––––––––––––––———
    // MARK: - View life cycle
    //––––––––––––––––––––––———

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    lazy var player: AVPlayer = {
        guard let clipURL = Bundle.main.url(forResource: "twitch_clip", withExtension: "mp4") else { return AVPlayer() }
        let player = AVPlayer(url: clipURL)
        return player
    }()

    lazy var playerLayer: AVPlayerLayer = {
        let playerLayer = AVPlayerLayer(player: player)
        return playerLayer
    }()

    lazy var videoPlayerView: VideoPlayerView = {
        let videoPlayerView = VideoPlayerView(frame: .zero)
        videoPlayerView.player = player
        return videoPlayerView
    }()

    lazy var webcamSelectAreaButton: SecondaryButton = {
        let button = SecondaryButton(frame: .zero, title: "Webcam", textColor: .foreground1, backgroundColor: .background2, selectedTextColor: .buttonBackround, selectedBackgroundColor: .foreground1, cornerRadius: 8) { [weak self] isSelected in
            if isSelected {
                self?.screenState = .webcam
            }
        }
        button.isSelectedState = true
        return button
    }()

    lazy var contentSelectAreaButton: SecondaryButton = {
        let button = SecondaryButton(frame: .zero, title: "Content", textColor: .foreground1, backgroundColor: .background2, selectedTextColor: .buttonBackround, selectedBackgroundColor: .foreground1, cornerRadius: 8) { [weak self] isSelected in
            if isSelected {
                self?.screenState = .content
            }
        }
        return button
    }()

    lazy var selectClipAreaContainer: UIView = {
        let stackView = UIStackView(arrangedSubviews: [webcamSelectAreaButton, contentSelectAreaButton])
        
        stackView.spacing = 20
        stackView.axis = .horizontal
        return stackView
    }()

    lazy var selectWebcamShapeLabel: UILabel = {
        let label = UILabel()
        label.text = "Select webcam shape"
        label.textColor = .foreground1
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return label
    }()

    lazy var webcamShapeSelectionView: UIHostingController<WebcamShapeSelectionView> = {
        let selectWebcamShapeView = WebcamShapeSelectionView(selectedShape: selectedWebcamShape) { [weak self] shape in
            self?.selectedWebcamShape = shape
        }
        let hostedSelectWebcamShapeView = UIHostingController(rootView: selectWebcamShapeView)
        hostedSelectWebcamShapeView.view.backgroundColor = .clear
        return hostedSelectWebcamShapeView
    }()

    lazy var contentShapeSelectionView: UIHostingController<ContentShapeSelectionView> = {
        let contentShapeSelectionView = ContentShapeSelectionView()
        let hostedContentShapeSelectionView = UIHostingController(rootView: contentShapeSelectionView)
        hostedContentShapeSelectionView.view.backgroundColor = .clear
        return hostedContentShapeSelectionView
    }()

    lazy var selectShapeContainter: UIView = {
        let view = UIView()
        view.addSubview(webcamShapeSelectionView.view)
        webcamShapeSelectionView.view.fillSuperview()
        return view
    }()

    lazy var adjustShapeSizeLabel: UILabel = {
        let label = UILabel()
        label.text = "Adjust shape size"
        label.textColor = .foreground1
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return label
    }()

    lazy var shapeSizeSlider: UISlider = {
        let slider = UISlider()
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        slider.tintColor = .buttonBackround
        slider.thumbTintColor = .foreground1
        slider.value = webcamShapeSliderValue
        return slider
    }()

    lazy var nextButton: PrimaryButton = {
        let nextButton = PrimaryButton(frame: .zero, title: "Next", cornerRadius: 15, action: nextButtonAction)
        return nextButton
    }()

    lazy var contentSelectionDraggableView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 3
        view.layer.borderColor = UIColor.yellowBrand?.cgColor
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan))
        view.addGestureRecognizer(panGestureRecognizer)
        return view
    }()

    func setupUI() {
        view.backgroundColor = .black
        setupNavigationBar()

        view.addSubview(videoPlayerView)
        videoPlayerView.anchor(top: safeTopAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 24, paddingLeft: 0, paddingRight: 0, height: 223)

        contentShapeLocation = videoPlayerView.center

        webcamSelectAreaButton.anchor(width: 165, height: 48)
        contentSelectAreaButton.anchor(width: 165, height: 48)

        view.addSubview(selectClipAreaContainer)
        selectClipAreaContainer.anchor(top: videoPlayerView.bottomAnchor, paddingTop: 28)
        selectClipAreaContainer.anchorCenterXToSuperview()

        view.addSubview(selectWebcamShapeLabel)
        selectWebcamShapeLabel.anchor(top: selectClipAreaContainer.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 30, paddingLeft: 20, paddingRight: 20, height: 22)

        view.addSubview(selectShapeContainter)
        selectShapeContainter.anchor(top: selectWebcamShapeLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 12, paddingLeft: 20, paddingRight: 20, height: 62)

        view.addSubview(adjustShapeSizeLabel)
        adjustShapeSizeLabel.anchor(top: selectShapeContainter.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 20, paddingLeft: 20, paddingRight: 20)

        view.addSubview(shapeSizeSlider)
        shapeSizeSlider.anchor(top: adjustShapeSizeLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 13, paddingLeft: 16, paddingRight: 16, height: 28)

        view.addSubview(nextButton)
        nextButton.anchor(left: view.leftAnchor, bottom: safeBottomAnchor, right: view.rightAnchor, paddingLeft: 32, paddingBottom: 0, paddingRight: 32, height: 48)

        view.addSubview(contentSelectionDraggableView)
        contentSelectionDraggableView.frame = .init(origin: videoPlayerView.center, size: contentShapeSize)
        view.layoutIfNeeded()
    }

    private func setupNavigationBar() {
        navigationItem.largeTitleDisplayMode = .never
        title = "Select clip areas"
    }

    @objc
    private func sliderValueChanged(slider: UISlider) {
        switch screenState {
        case .webcam:
            webcamShapeSliderValue = slider.value
        case .content:
            contentShapeSliderValue = slider.value
        }
    }

    private func nextButtonAction() {

    }

    @objc
    private func didPan(_ sender: UIPanGestureRecognizer) {
        let newLocation = sender.location(in: videoPlayerView)
        if (newLocation.x - (contentShapeSize.width / 2)) > videoPlayerView.frame.minX && (newLocation.x + (contentShapeSize.width / 2)) < videoPlayerView.frame.maxX {
            contentSelectionDraggableView.center.x = sender.location(in: videoPlayerView).x
        }
        if (newLocation.y - (contentShapeSize.height / 2)) > videoPlayerView.frame.minY && (newLocation.y + (contentShapeSize.height/2)) < videoPlayerView.frame.maxY {
            contentSelectionDraggableView.center.y = sender.location(in: videoPlayerView).y
        }
        contentShapeLocation = contentSelectionDraggableView.center
    }
}
