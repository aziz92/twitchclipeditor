import SwiftUI

struct WebcamShapeSelectionView: View {

    @State var selectedShape: SelectClipAreasViewController.WebcamShape
    var selectedShapeIsChanged: (SelectClipAreasViewController.WebcamShape) -> Void

    var body: some View {
        HStack(spacing: 20) {
            Rectangle()
                .stroke(selectedShape == .square ? Color.buttonBackround : .foreground3, lineWidth: 2)
                .frame(width: 35, height: 35)
                .onTapGesture { selectedShape = .square }
            Rectangle()
                .stroke(selectedShape == .rectangle ? Color.buttonBackround : .foreground3, lineWidth: 2)
                .frame(width: 62, height: 35)
                .onTapGesture { selectedShape = .rectangle }
            Circle()
                .stroke(selectedShape == .circle ? Color.buttonBackround : .foreground3, lineWidth: 2)
                .frame(width: 35, height: 35)
                .onTapGesture { selectedShape = .circle }
            Spacer()
        }
        .background(.clear)
        .onChange(of: selectedShape) { newValue in
            selectedShapeIsChanged(newValue)
        }
    }
}
