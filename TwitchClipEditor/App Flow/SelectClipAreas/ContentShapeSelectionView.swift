import SwiftUI

struct ContentShapeSelectionView: View {

    var body: some View {
        HStack(spacing: 20) {
            Rectangle()
                .stroke(Color.buttonBackround, lineWidth: 2)
                .frame(width: 35, height: 62)
            Spacer()
        }
        .background(.clear)
    }
}
