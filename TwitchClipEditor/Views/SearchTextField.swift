import UIKit

class SearchTextField: UIView {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    init(frame: CGRect, onSearchButtonTapped: @escaping (String) -> Void, onTextChanged: @escaping () -> Void) {
        self.onSearchButtonTapped = onSearchButtonTapped
        self.onTextChanged = onTextChanged

        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    //––––––––––––––––––––––———
    // MARK: - Closures
    //––––––––––––––––––––––———

    var onSearchButtonTapped: ((String) -> Void)?
    var onTextChanged: (() -> Void)?

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    lazy var searchTextField: UITextField = {
        let searchTextField = UITextField()
        searchTextField.textColor = .foreground2
        searchTextField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        let attributedPlaceholder: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.foreground2 as Any]
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Enter twitch video url...", attributes: attributedPlaceholder)
        searchTextField.returnKeyType = .continue
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return searchTextField
    }()

    private lazy var searchButton: UIButton = {
        let searchButton = UIButton(type: .system)
        searchButton.addTarget(self, action: #selector(searchButtonTapped), for: .touchUpInside)

        let image = UIImage.searchIcon
        searchButton.setImage(.searchIcon?.withTintColor(.foreground2 ?? .gray, renderingMode: .alwaysOriginal), for: .normal)
        searchButton.imageView?.contentMode = .scaleAspectFill

        return searchButton
    }()

    private func setupView() {
        backgroundColor = .twitchPurple

        let horizontalStackView = UIStackView(arrangedSubviews: [searchTextField, searchButton])
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 40
        horizontalStackView.alignment = .center

        self.addSubview(horizontalStackView)

        horizontalStackView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 12, paddingLeft: 16, paddingBottom: 12, paddingRight: 12)
        searchButton.anchor(width: 24)
    }

    @objc private func searchButtonTapped() {
        guard let text = searchTextField.text else { return }
        onSearchButtonTapped?(text)
        searchTextField.resignFirstResponder()
    }
}

extension SearchTextField: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchButtonTapped()
        return false
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        onTextChanged?()
    }
}
