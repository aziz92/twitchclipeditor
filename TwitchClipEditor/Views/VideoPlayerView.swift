import UIKit
import AVFoundation

class VideoPlayerView: UIView {

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––

    var playerLayer: AVPlayerLayer?

    var player: AVPlayer? {
        didSet {
            playerLayer?.removeFromSuperlayer()
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = bounds
            playerLayer?.videoGravity = .resizeAspectFill
            layer.insertSublayer(playerLayer!, at: 0)
            player?.play()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer?.frame = bounds
        addOnEndObserver()
    }

    private func addOnEndObserver() {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem, queue: .main) { [weak player] _ in
            player?.seek(to: .zero)
            player?.play()
        }
    }
}
