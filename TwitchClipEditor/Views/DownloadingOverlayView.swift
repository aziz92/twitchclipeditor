import UIKit

class DownloadingOverlayView: UIView {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––

    var downloadingPercentage: Int = 100 {
        didSet {
            downloadingLabel.text = "Downloading: \(downloadingPercentage)%"
        }
    }

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    lazy var spinnerView: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .medium)
        spinner.backgroundColor = .clear
        spinner.color = .foreground1
        spinner.startAnimating()
        spinner.anchor(width: 24, height: 24)
        return spinner
    }()

    lazy var downloadingLabel: UILabel = {
        let downloadingLabel = UILabel(frame: .zero)
        downloadingLabel.text = "Downloading: \(downloadingPercentage)%"
        downloadingLabel.textColor = .foreground1
        downloadingLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        downloadingLabel.numberOfLines = 1
        downloadingLabel.backgroundColor = .clear
        return downloadingLabel
    }()

    lazy var downloadingStackView: UIStackView = {
        let downloadingStackView = UIStackView(arrangedSubviews: [spinnerView, downloadingLabel])
        downloadingStackView.axis = .horizontal
        downloadingStackView.spacing = 8
        downloadingStackView.alignment = .center
        return downloadingStackView
    }()

    lazy var downloadingInfoView: UIView = {
        let downloadingView = UIView(frame: .init(x: 0, y: 0, width: 233, height: 42))
        downloadingView.backgroundColor = .twitchPurple
        downloadingView.layer.cornerRadius = 17
        return downloadingView
    }()

    lazy var overlayView: UIView = {
        let overlayView = UIView(frame: .zero)
        overlayView.backgroundColor = .overlay1
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.insetsLayoutMarginsFromSafeArea = false
        return overlayView
    }()

    func setupUI() {
        setupDownloadingOverlayView()
    }

    private func setupDownloadingOverlayView() {
        self.addSubview(overlayView)
        overlayView.addSubview(downloadingInfoView)
        downloadingInfoView.addSubview(downloadingStackView)

        overlayView.fillSuperview()
        downloadingInfoView.anchor(width: 233, height: 42)
        downloadingInfoView.centerInSuperview()

        downloadingStackView.centerInSuperview()
    }
}
