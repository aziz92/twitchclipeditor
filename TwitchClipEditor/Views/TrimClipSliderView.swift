import AVFoundation
import SwiftUI

struct TrimClipSliderView: View {

    let frames: [UIImage]
    let asset: AVAsset
    let player: AVPlayer

    @Binding var offsetLeft: CGFloat
    @Binding var offsetRight: CGFloat
    @Binding var width: CGFloat
    @Binding var playing: Bool

    @State var isSliderSelected: Bool = false

    var body: some View {
        HStack(spacing: 0) {
            Button(action: playButtonAction) {
                if playing {
                    Image(systemName: "pause.fill")
                        .resizable()
                        .frame(width: 18, height: 20)
                        .foregroundColor(.white)
                } else {
                    Image("play_button")
                        .resizable()
                        .frame(width: 18, height: 20)
                        .foregroundColor(.white)
                }
            }
            .padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 10))
            Divider()
                .overlay(Color(UIColor(hex: "B0B0B0")))
            rangeSlider
                .padding(EdgeInsets(top: 5, leading: 8, bottom: 5, trailing: 8))
        }
        .frame(height: 50)
        .background(Color.sliderBackground)
        .cornerRadius(8)
        .onAppear {
            player.play()
            startPeriodicTimeObserver()
        }
        .onDisappear {
            player.pause()
        }
    }

    private var rangeSlider: some View {
        HStack(spacing: 0) {
            ForEach(frames, id: \.self) { frame in
                Image(uiImage: frame)
                    .resizable()
            }
        }
        .frame(height: 34)
        .padding(.horizontal, 26)
        .overlay { slider }
        .padding(.vertical, 5)
        .background {
            if isSliderSelected {
                HStack(spacing: 0) {
                    Color.clear
                        .frame(width: offsetLeft)
                    Color.yellow
                        .cornerRadius(6)
                    Color.clear
                        .frame(width: offsetRight)
                }
            }
        }
    }

    private var slider: some View {
        GeometryReader { geo in
            Rectangle()
                .fill(Color.clear)
                .opacity(0.5)
                .contentShape(Rectangle())
                .gesture(
                    DragGesture()
                        .onChanged{ gesture in
                            let location = gesture.location.x
                            updateOffsets(location: location, width: geo.size.width)
                        }
                )
                .overlay {
                    HStack(spacing: 0) {
                        Group {
                            Color.black.opacity(0.2)
                                .padding(.leading, 26)
                        }
                        .frame(width: offsetLeft)
                        HStack(spacing: 0) {
                            Image("left_arrow")
                                .resizable()
                                .frame(width: 11, height: 24)
                                .foregroundColor(isSliderSelected ? .black : .white)
                                .padding(8)
                                .background(isSliderSelected ? Color.yellow : .clear)
                                .cornerRadius(6)
                            Spacer()
                            Image("right_arrow")
                                .resizable()
                                .frame(width: 11, height: 24)
                                .foregroundColor(isSliderSelected ? .black : .white)
                                .padding(8)
                                .background(isSliderSelected ? Color.yellow : .clear)
                                .cornerRadius(6)
                        }
                        Group {
                            Color.black.opacity(0.2)
                                .padding(.trailing, 26)
                        }
                        .frame(width: offsetRight)
                    }
                    .allowsHitTesting(false)
                }
        }
    }

    private func playButtonAction() {
        if playing {
            player.pause()
        }
        else {
            Task {
                do {
                    let durationSeconds = try await getDuration()
                    if let currentItem = player.currentItem {
                        let currentTime = CMTimeGetSeconds(currentItem.currentTime())
                        if (currentTime + 1) >= ((width - offsetRight) / width * durationSeconds) { // if the current time + 1 (because if the current time is 24 second and 3 miliseconds it will return 24) of the clip is at the end
                            seekTo(pos: offsetLeft/width)
                        }
                    }
                    player.play()
                } catch {
                    print(error.localizedDescription)
                }

            }
        }
        playing.toggle()
    }

    private func updateOffsets(location: CGFloat, width: CGFloat) {

        player.pause()
        playing = false
        self.width = width

        //check which side are we moving
        let diffLeft = abs(offsetLeft - location)
        let diffRight = abs((width - offsetRight) - location)

        if (diffLeft < diffRight) {
            guard location + 64 < width - offsetRight else { return }
            offsetLeft = max(0, location)
            seekTo(pos: offsetLeft/width)
        } else {
            guard location - 64 > offsetLeft else { return }
            offsetRight = max(0, width - location)
            seekTo(pos: (width - offsetRight)/width)
        }

        isSliderSelected = offsetLeft != 0 || offsetRight != 0
    }

    private func getDuration() async throws-> CGFloat {
        let duration: CMTime = try await asset.load(.duration)
        let durationInSecs  = CMTimeGetSeconds(duration)
        return durationInSecs
    }

    private func seekTo(pos: CGFloat) {
        Task {
            do {
                let durationSecs = try await Int(getDuration())

                let time: CMTime = CMTimeMakeWithSeconds(CGFloat(durationSecs) * pos, preferredTimescale: player.currentTime().timescale)
                await player.seek(to: time, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }

    private func startPeriodicTimeObserver() {
        player.addPeriodicTimeObserver(forInterval: CMTime(seconds:0.1, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), queue: nil) { time in
            Task {
                guard playing else { return }
                let durationSeconds = try? await getDuration()
                guard let durationSeconds else { return }
                let currentTime = CMTimeGetSeconds(time)
                if durationSeconds == currentTime {
                    player.pause()
                    playing = false
                    seekTo(pos: width == 0 ? 0 : offsetLeft/width)
                }
                if (offsetRight != 0 && currentTime >= ((width - offsetRight) / width * durationSeconds)) {
                    player.pause()
                    playing = false
                    seekTo(pos: offsetLeft/width)
                }
            }

        }
    }
}
