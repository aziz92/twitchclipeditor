import UIKit

class ValidationSearchTextField: UIView {
    
    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    init(frame: CGRect, isValid: Bool = true, validatorCriteria: ValidatorCriteria, onSearchButtonTapped: @escaping ((String) -> Void)) {
        self.isValid = isValid
        self.validatorCriteria = validatorCriteria
        self.onSearchButtonTapped = onSearchButtonTapped
        super.init(frame: frame)

        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––

    var isValid: Bool {
        didSet {
            validationLabel.isHidden = isValid
        }
    }

    var validatorCriteria: ValidatorCriteria

    //––––––––––––––––––––––———
    // MARK: - Closures
    //––––––––––––––––––––––———

    var onSearchButtonTapped: ((String) -> Void)

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    lazy var searchTextField: SearchTextField = {
        let searchTextField = SearchTextField(frame: .zero, onSearchButtonTapped: searchButtonTapped(text:), onTextChanged: onTextChanged)
        searchTextField.layer.cornerRadius = 15
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        return searchTextField
    }()

    lazy private var validationLabel: UILabel = {
        let validationLabel = UILabel()
        validationLabel.textColor = .systemRed
        validationLabel.text = "Url is not valid!"
        validationLabel.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        validationLabel.translatesAutoresizingMaskIntoConstraints = false
        validationLabel.isHidden = true
        return validationLabel
    }()

    private func setupView() {
        self.addSubview(searchTextField)
        self.addSubview(validationLabel)

        searchTextField.anchor(top: topAnchor, left: leftAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingRight: 0, height: 47)
        validationLabel.anchor(top: searchTextField.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 4, paddingLeft: 8, paddingBottom: 0, paddingRight: 0)
    }

    private func searchButtonTapped(text: String) {
        guard validatorCriteria.isValid(text: text) else {
            isValid = false
            return
        }
        onSearchButtonTapped(text)
    }

    private func onTextChanged() {
        isValid = true
    }
}
