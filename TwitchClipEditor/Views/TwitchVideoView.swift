import SwiftUI
import AVFoundation

struct TwitchVideoView: View {

    @State var player: AVPlayer
    @Binding var linkText: String
    let videoAspectRatio: Double?

    @State private var showingAlert = false

    var body: some View {
        ZStack(alignment: .center) {
            CustomVideoPlayerView(player: player, videoAspectRatio: videoAspectRatio)
                .background(Color.blue)
            HStack(spacing: 6) {
                Image("twitch_icon")
                    .resizable()
                    .frame(width: 21, height: 21)
                Text(linkText)
                    .foregroundColor(.foreground1)
                    .font(.system(size: 12.5, weight: .semibold))
                Spacer()
            }
            .offset(y: (UIScreen.main.bounds.width / 2) * (1 / (videoAspectRatio ?? 1)) + 22)
            .onTapGesture {
                showingAlert.toggle()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding(.horizontal, 12)
        }
        .ignoresSafeArea()
        .background(Color.red)
        .alert("Enter link:", isPresented: $showingAlert) {
            TextField("Enter your link:", text: $linkText)
            Button("OK") {
                showingAlert.toggle()
            }
        }
    }
}
