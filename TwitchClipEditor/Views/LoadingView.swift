import UIKit

final class LoadingView: UIView {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    var spinnerView: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.backgroundColor = .clear
        spinner.color = .foreground1
        spinner.startAnimating()
        spinner.anchor(width: 32, height: 32)
        return spinner
    }()


    var loadingView: UIView = {
        let loadingView = UIView(frame: .init(x: 0, y: 0, width: 64, height: 64))
        loadingView.backgroundColor = .twitchPurple
        loadingView.layer.cornerRadius = 16
        return loadingView
    }()

    var overlayView: UIView = {
        let overlayView = UIView(frame: .zero)
        overlayView.backgroundColor = .overlay1
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.insetsLayoutMarginsFromSafeArea = false
        return overlayView
    }()

    private func setupUI() {
        addSubview(overlayView)
        overlayView.addSubview(loadingView)
        loadingView.addSubview(spinnerView)

        overlayView.fillSuperview()
        loadingView.anchor(width: 64, height: 64)
        loadingView.centerInSuperview()

        spinnerView.centerInSuperview()
    }

    func show() {
        alpha = 1
    }

    func hide() {
        alpha = 0
    }
}
