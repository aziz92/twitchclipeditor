import AVFoundation
import AVKit
import SwiftUI

struct AVPlayerControllerRepresented : UIViewControllerRepresentable {
    var player : AVPlayer

    func makeUIViewController(context: Context) -> AVPlayerViewController {
        let controller = AVPlayerViewController()
        controller.player = player
        controller.showsPlaybackControls = false
        controller.allowsVideoFrameAnalysis = false

        return controller
    }

    func updateUIViewController(_ uiViewController: AVPlayerViewController, context: Context) {
        uiViewController.view.layoutIfNeeded()
    }
}
