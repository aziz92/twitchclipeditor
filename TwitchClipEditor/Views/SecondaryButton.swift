import UIKit

class SecondaryButton: UIButton {

    var isSelectedState = false {
        didSet {
            updateUI()
        }
    }

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    init(frame: CGRect, title: String, textColor: UIColor?, backgroundColor: UIColor?, selectedTextColor: UIColor?, selectedBackgroundColor: UIColor?, cornerRadius: CGFloat, action: @escaping (Bool) -> Void) {
        self.action = action
        normalTitleColor = textColor
        selectedTitleColor = selectedTextColor
        normalBackgroundColor = backgroundColor
        self.selectedBackgroundColor = selectedBackgroundColor
        super.init(frame: frame)

        setTitle(title, for: .normal)
        setTitleColor(textColor, for: .normal)

        self.backgroundColor = backgroundColor
        layer.cornerRadius = cornerRadius
        addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Properties
    //––––––––––––––––––––––

    let normalTitleColor: UIColor?
    let selectedTitleColor: UIColor?
    let normalBackgroundColor: UIColor?
    let selectedBackgroundColor: UIColor?

    //––––––––––––––––––––––
    // MARK: - Closures
    //––––––––––––––––––––––

    let action: (Bool) -> Void

    @objc private func buttonAction() {
        guard !isSelectedState else { return }
        isSelectedState = true
        action(isSelectedState)
    }

    private func updateUI() {
        setTitleColor(isSelectedState ? selectedTitleColor : normalTitleColor, for: .normal)
        self.backgroundColor = isSelectedState ? selectedBackgroundColor : normalBackgroundColor
    }
}
