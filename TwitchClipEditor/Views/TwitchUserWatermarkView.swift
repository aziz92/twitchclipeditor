import UIKit

class TwitchUserWatermarkView: UIView {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––

    init(frame: CGRect, watermarkText: String) {
        self.watermarkText = watermarkText
        super.init(frame: frame)

        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Variables
    //––––––––––––––––––––––

    var watermarkText: String {
        didSet {
            watermarkLabel.text = watermarkText
        }
    }

    //––––––––––––––––––––––———
    // MARK: - Setup UI
    //––––––––––––––––––––––———

    private lazy var logoImage: UIImageView = {
        let imageView = UIImageView(image: .twitchIcon)
        return imageView
    }()

    private lazy var watermarkLabel: UILabel = {
        let watermarkLabel = UILabel()
        watermarkLabel.text = watermarkText
        watermarkLabel.textColor = .foreground1
        watermarkLabel.font = UIFont.systemFont(ofSize: 6, weight: .bold)
        return watermarkLabel
    }()

    private func setupUI() {
        addSubview(logoImage)
        addSubview(watermarkLabel)

        logoImage.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, width: 8.5, height: 10)
        watermarkLabel.anchor(left: logoImage.rightAnchor, right: rightAnchor, paddingLeft: 4)
        watermarkLabel.anchorCenterYToSuperview()
    }
}
