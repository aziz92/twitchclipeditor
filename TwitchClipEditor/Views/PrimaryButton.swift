import UIKit

class PrimaryButton: UIButton {

    //––––––––––––––––––––––
    // MARK: - Initializers
    //––––––––––––––––––––––
    
    init(frame: CGRect, title: String, textColor: UIColor? = .foreground1, backgroundColor: UIColor? = .buttonBackround, cornerRadius: CGFloat = 8.0, action: @escaping () -> Void) {
        self.action = action
        super.init(frame: frame)

        setTitle(title, for: .normal)
        setTitleColor(textColor, for: .normal)
        self.backgroundColor = backgroundColor
        layer.cornerRadius = cornerRadius
        addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //––––––––––––––––––––––
    // MARK: - Closures
    //––––––––––––––––––––––

    let action: () -> Void

    @objc private func buttonAction() {
        action()
    }

}
