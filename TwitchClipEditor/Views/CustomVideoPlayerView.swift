import AVFoundation
import AVKit
import SwiftUI

struct CustomVideoPlayerView: View {

    let player: AVPlayer
    let videoAspectRatio: Double?

    var body: some View {
        VideoPlayer(player: player)
            .edgesIgnoringSafeArea(.all)
            .allowsHitTesting(false) // Disables user interaction
            .overlay(VideoControlsOverlay(), alignment: .bottom) // Adds custom overlay for controls
            .onAppear {
                player.play()
            }
    }

    struct VideoControlsOverlay: View {
        var body: some View {
            // Add your custom overlay for controls here
            // For example, you can add buttons or other UI elements to control the playback state of AVPlayer
            // You can also add any additional customizations or styling as needed
            Rectangle()
                .opacity(0.01) // Set opacity to a low value to make the overlay invisible
        }
    }
}
