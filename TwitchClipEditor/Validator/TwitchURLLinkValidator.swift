struct TwitchURLLinkValidator: ValidatorCriteria {

    func isValid(text: String) -> Bool {
        text.lowercased().contains("twitch")
    }
}
