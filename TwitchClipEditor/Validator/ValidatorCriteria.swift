protocol ValidatorCriteria {
    func isValid(text: String) -> Bool
}
