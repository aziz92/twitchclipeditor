import UIKit

extension UIView {

    func addBlurEffect(style: UIBlurEffect.Style = .light, alpha: CGFloat = 1.0) {
        let blurEffect = UIBlurEffect(style: style)
        let visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.alpha = alpha
        visualEffectView.frame = bounds
        addSubview(visualEffectView)
    }
}
