import UIKit
import SwiftUI

extension UIColor {

    static let background1      = UIColor(named: "background1")
    static let background2      = UIColor(named: "background2")
    static let blackBackground  = UIColor(named: "black_background")
    static let buttonBackround  = UIColor(named: "button_background")
    static let foreground1      = UIColor(named: "foreground1")
    static let foreground2      = UIColor(named: "foreground2")
    static let overlay1         = UIColor(named: "overlay1")
    static let twitchPurple     = UIColor(named: "twitch_purple")
    static let yellowBrand      = UIColor(named: "yellow_brand")
}

extension Color {

    static let buttonBackround  = Color("button_background")
    static let foreground1      = Color("foreground1")
    static let sliderBackground = Color("slider_background")
    static let foreground3      = Color("foreground3")
}
