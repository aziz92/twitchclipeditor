import UIKit

extension UIView {
    
   func roundCorners(radius: CGFloat) {
       layer.cornerRadius = radius
       layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
}
