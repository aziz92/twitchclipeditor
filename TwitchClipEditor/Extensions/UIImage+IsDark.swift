import UIKit

extension UIImage {
    
    func isDark() -> Bool {
        guard let imageData = self.jpegData(compressionQuality: 1.0),
              let dataProvider = CGDataProvider(data: imageData as CFData),
              let cgImage = CGImage(jpegDataProviderSource: dataProvider, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
        else { return true } // Assume image is dark if we can't read the data

        let width = cgImage.width
        let height = cgImage.height
        let bitsPerComponent = cgImage.bitsPerComponent
        let bytesPerRow = cgImage.bytesPerRow
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = cgImage.bitmapInfo

        guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue),
              let pixelData = context.data?.assumingMemoryBound(to: UInt8.self)
        else { return true } // Assume image is dark if we can't create the context or get pixel data

        context.draw(cgImage, in: CGRect(x: 0, y: 0, width: width, height: height))

        var totalBrightness: CGFloat = 0

        for y in 0..<height {
            for x in 0..<width {
                let offset = 4 * (y * width + x)
                let r = CGFloat(pixelData[offset]) / 255.0
                let g = CGFloat(pixelData[offset + 1]) / 255.0
                let b = CGFloat(pixelData[offset + 2]) / 255.0
                let a = CGFloat(pixelData[offset + 3]) / 255.0
                let brightness = (r + g + b) / 3 * a
                totalBrightness += brightness
            }
        }

        let averageBrightness = totalBrightness / CGFloat(width * height)
        return averageBrightness < 0.5 // Return true if the average brightness is less than 0.5, i.e. the image is dark
    }
}
