import UIKit

extension UIImage {
    
    static let searchIcon   = UIImage(named: "search_icon")
    static let twitchIcon   = UIImage(named: "twitch_icon")
}
