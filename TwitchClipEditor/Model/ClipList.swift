//
//  ClipList.swift
//  StreamTracker
//
//  Created by az on 28/03/2023.
//  Copyright © 2023 az. All rights reserved.
//

import Foundation


// MARK: - ClipList
struct ClipList: Codable {
    let clips: [Clip]
    
    enum CodingKeys: String, CodingKey {
        case clips = "data"
    }
}

// MARK: - Clip (with only the data needed to parse. Full reference available here: https://dev.twitch.tv/docs/api/clips/#getting-clips

struct Clip: Codable {
    let id: String
    let url: String
    let broadcasterId, broadcasterName: String
    let title: String
    let createdAt: String
    let thumbnailUrl: String
    let duration: Int

    enum CodingKeys: String, CodingKey {
        case id, url
        case broadcasterId = "broadcaster_id"
        case broadcasterName = "broadcaster_name"
        case title
        case createdAt = "created_at"
        case thumbnailUrl = "thumbnail_url"
        case duration
    }
    
    func getVideoDataUrl() -> String? {
        //implement function to convert thumbnailURL into videoURL
        return nil
    }
}

/*
Example JSON blob for a single Clip
 {
   "data": [
     {
       "id": "AwkwardHelplessSalamanderSwiftRage",
       "url": "https://clips.twitch.tv/AwkwardHelplessSalamanderSwiftRage",
       "embed_url": "https://clips.twitch.tv/embed?clip=AwkwardHelplessSalamanderSwiftRage",
       "broadcaster_id": "67955580",
       "broadcaster_name": "ChewieMelodies",
       "creator_id": "53834192",
       "creator_name": "BlackNova03",
       "video_id": "205586603",
       "game_id": "488191",
       "language": "en",
       "title": "babymetal",
       "view_count": 10,
       "created_at": "2017-11-30T22:34:18Z",
       "thumbnail_url": "https://clips-media-assets.twitch.tv/157589949-preview-480x272.jpg",
       "duration": 60,
       "vod_offset": 480
     }
   ]
 }
 
 */
