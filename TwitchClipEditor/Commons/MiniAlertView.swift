//
//  MiniAlertView.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

import Foundation
import UIKit

class MiniAlertView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        layer.cornerRadius = 10
        setupUI()
    }
    
    convenience init(text: String) {
        self.init()
        label.text = text
    }
    
    let blurredBackgroundView: UIVisualEffectView = {
        let bv = UIVisualEffectView()
        bv.frame = CGRect.zero
        bv.effect = UIBlurEffect(style: .light)
        bv.layer.cornerRadius = 10
        bv.clipsToBounds = true
        return bv
    }()
    
    lazy var label: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.text = ""
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.adjustsFontSizeToFitWidth = true
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        return lbl
    }()
    
    func setupUI() {
        
        alpha = 0
        addSubview(blurredBackgroundView)
        addSubview(label)
        
        blurredBackgroundView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
