//
//  CustomHeader.swift
//  TwitchClipEditor
//
//  Created by az on 28/03/2023.
//

import Foundation
import UIKit

class CustomHeader: UICollectionReusableView {
    
    // ————————————————
    // MARK: Init
    // ————————————————
    
    override init(frame: CGRect) {
        super.init(frame: frame)
            setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // ————————————————
    // MARK: Setup UI
    // ————————————————
    
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .label
        lbl.textAlignment = .left
        lbl.numberOfLines = 1
        lbl.font = UIFont.systemFont(ofSize: 30, weight: .heavy)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.backgroundColor = .clear
        return lbl
    }()
    
    
    
    func setupUI() {
        
        backgroundColor = .clear

        addSubview(titleLbl)
        titleLbl.fillSuperview()
    }
    
    func setup(title: String) {
        self.titleLbl.text = title
    }
   
    override func prepareForReuse() {
        self.titleLbl.text = ""
    }
    
}
