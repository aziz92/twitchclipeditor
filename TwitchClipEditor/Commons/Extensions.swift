//
//  Extensions.swift
//  StoryStickers
//
//  Created by az on 05/03/2019.
//  Copyright © 2019 az. All rights reserved.
//

import Foundation
import UIKit



extension Int {
    func toStringWithDecimals() -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))
    }
    
    func formatCurrentCount() -> String {
        
        if self > 999999 {
            if self % 1000000 <= 100000 {
                return "\(self/1000000)M"
            }
            return String(format: "%.1f", CGFloat(self)/1000000.0) + "M"
        }
        
        if self > 99999 {
            if self % 1000 <= 100 {
                return "\(self/1000)K"
            }
        }
        
        if self > 9999 {
            if self % 1000 <= 100 {
                return "\(self/1000)K"
            }
            return String(format: "%.1f", CGFloat(self)/1000.0) + "K"
        }
        if let countStr = self.toStringWithDecimals() {
            return countStr
        }
        return "\(String(describing: self))"
    }
}

extension CGFloat {
    
    
    var toDegrees: CGFloat {
        return self * 180 / .pi
    }
    
    var toRadiants: CGFloat {
        return self * .pi / 180
    }
    
    
    func inchToCM() -> Double {
        return Double(self / 0.39370)
    }
    
    func cmToInch() -> Double {
        return Double(self * 0.39370)
    }
    
    func feetToFeetInch() -> (feet: Double, inch: Double) {
        let feet = Int(self / 12)
        let inch = Int(self.truncatingRemainder(dividingBy: 12.0))
        return (Double(feet), Double(inch))
    }

    func scaled() -> CGFloat {
        let currentScreenHeight = UIScreen.main.nativeBounds.height
        let scaleFactor = currentScreenHeight / 1920
        return self * scaleFactor
    }
    
    func scaledBasedOnSuperView(superViewHeight: CGFloat) -> CGFloat {
        let currentScreenHeight = UIScreen.main.nativeBounds.height
        let scaleFactor = (currentScreenHeight / 1920)
        return self * scaleFactor * (superViewHeight / currentScreenHeight)
    }
    
    func toStringPercentageWith(decimal: Int) -> String {
        let format = "%.\(decimal)f%%"
        print("Formatting: \(self) to a percentage.")
        return String(format: format, self * 100.0)
    }
    
}

extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    var maxOneDecimal: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.1f", self)
    }
}

extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}

extension String {
    
    var youtubeVideoId: String? {
        let typePattern = "(?:(?:\\.be\\/|embed\\/|v\\/|\\?v=|\\&v=|\\/videos\\/)|(?:[\\w+]+#\\w\\/\\w(?:\\/[\\w]+)?\\/\\w\\/))([\\w-_]+)"
        let regex = try? NSRegularExpression(pattern: typePattern, options: .caseInsensitive)
        return regex
            .flatMap { $0.firstMatch(in: self, range: NSMakeRange(0, self.count)) }
            .flatMap { Range($0.range(at: 1), in: self) }
            .map { String(self[$0]) }
    }
    
    func matchesFor(regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: .caseInsensitive)
            
            let results = regex.matches(in: self, range: NSRange(self.startIndex..., in: self))
            
            return results.map {
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func isValidEmail() -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
    
    func toNSAttributedStringWithfontAttributeOf(size: CGFloat, weight: UIFont.Weight, textColor: UIColor) -> NSAttributedString {
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size, weight: weight), NSAttributedString.Key.foregroundColor: textColor]
        return NSAttributedString(string: self, attributes: attributes)
        
    }
    
    static let numberFormatter = NumberFormatter()
    var intValue: Int? {
        String.numberFormatter.decimalSeparator = ","
        if let result = String.numberFormatter.number(from: self){
            return result.intValue
        } else {
            return nil 
        }
    }
}

extension UIImage {
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x:0, y:0, width:size.width, height:size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    class func roundedImage(image: UIImage, cornerRadius: Int) -> UIImage {
        let rect = CGRect(origin:CGPoint(x: 0, y: 0), size: image.size)
        UIGraphicsBeginImageContextWithOptions(image.size, false, 1)
        UIBezierPath(
            roundedRect: rect,
            cornerRadius: CGFloat(cornerRadius)
            ).addClip()
        image.draw(in: rect)
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
}

extension CALayer {
    
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
            shouldRasterize = true
            rasterizationScale = UIScreen.main.scale
        }
    }
}


extension UIButton {
    func applyShrinkOnTap() {
        addTapTargets()
    }
    
    private func addTapTargets() {
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchUpOutside, .touchCancel])
        addTarget(self, action: #selector(touchDown), for: .touchDown)
    }
    
    @objc private func touchUp() {
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 1.5, options: [], animations: {
            self.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        })
    }
    
    @objc private func touchDown() {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.layer.transform = CATransform3DMakeScale(0.98,0.98,1.0)
        })
    }
}

extension UIViewController {
    
    func vcName()-> String {
        return "\(Self.self)"
    }
    
    var safeTopAnchor: NSLayoutYAxisAnchor {
        return view.safeAreaLayoutGuide.topAnchor
    }
    
    var safeBottomAnchor: NSLayoutYAxisAnchor {
        return view.safeAreaLayoutGuide.bottomAnchor
    }
    
    var isIpad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    //––––––––––––––––––––––————————————————
    // MARK: - Child ViewController helpers
    //––––––––––––––––––––––————————————————
    
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.translatesAutoresizingMaskIntoConstraints = false
    }

    
    func add(_ child: UIViewController, view: UIView) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func remove() {
        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.
        guard parent != nil else {
            return
        }
        
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    

}

extension UICollectionView {
    func reloadItems(inSection section:Int) {
        reloadItems(at: (0..<numberOfItems(inSection: section)).map {
            IndexPath(item: $0, section: section)
        })
    }
}

extension UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UIColor {
   
    static let lightGrayBackground  = UIColor(hex: "F5F5F5")
    static let greenBlue            = UIColor(hex: "00879F")
    static let subBlue              = UIColor(hex: "0A84FF")
    static let lightGreen           = UIColor(hex: "00C87F")
    static let customGray           = UIColor(hex: "7784A4")
    static let customLightGray      = UIColor(hex: "F0F0F0")
    static let customDarkGray       = UIColor(hex: "4B505C")
    static let customLightBlue      = UIColor(hex: "81A7FF")
    static let customPeach          = UIColor(hex: "F2B49D")
    static let customPink           = UIColor(hex: "F4828A")
    static let gradientLightBlue    = UIColor(hex: "ABE0FF")
    static let gradientBlue         = UIColor(hex: "005DC7")
    static let gradientLightPurple  = UIColor(hex: "6868B9")
    static let gradientDarkPurple   = UIColor(hex: "171E3C")
    static let lightModePurple      = UIColor(hex: "752BE9")
    static let darkBlue             = UIColor(hex: "2D17AF")
    static let darkModePurple       = UIColor(hex: "C094FF")
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    convenience init(hex: String, alpha: CGFloat = 1) {
        
        let hex: String = String(hex.dropFirst(hex.hasPrefix("#") ? 1 : 0))
        
        let scanner = Scanner(string: hex)
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: alpha
        )
    }
}

extension UIView {
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-10.0, 10.0, -10.0, 10.0, -5.0, 5.0, -2.0, 2.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func minimalShake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.4
        animation.values = [-8.0, 8.0, -5.0, 5.0, -2.0, 2.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func nanoShake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.3
        animation.values = [-4.0, 4.0, -2.0, 2.0, -0.5, 0.5, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func scale(values: [Float], duration: Double, repeatsForever: Bool, timingFunctionName: CAMediaTimingFunctionName = .linear) {
        let animation = CAKeyframeAnimation(keyPath: "transform.scale.xy")
        animation.timingFunction = CAMediaTimingFunction(name: timingFunctionName)
        animation.duration = duration
        animation.values = values
        animation.repeatCount = repeatsForever ? Float.infinity : 1
        layer.add(animation, forKey: "scale")
    }
    
    func toUIImage(isOpaque: Bool = false) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    func imaged() -> UIImage? {
        let renderer = UIGraphicsImageRenderer(size: bounds.size)
        
        let image = renderer.image { ctx in
            drawHierarchy(in: bounds, afterScreenUpdates: true)
        }
        return image
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
    
    func dropShadow(offsetX: CGFloat, offsetY: CGFloat, color: UIColor, opacity: Float, radius: CGFloat, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: offsetX, height: offsetY)
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // ––––––––––––––––––––––––————
    // Mark: Insert gradient layer
    // ––––––––––––––––––––––––————
    
    func insertGradientLayer(frame: CGRect, colors:[CGColor], startPoint: CGPoint, endPoint: CGPoint) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        gradientLayer.colors = colors
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        self.layer.addSublayer(gradientLayer)
    }
    
    //––––––––––––––––––––––———
    // MARK: - Autolayout
    //––––––––––––––––––––––———

    func anchor(top: NSLayoutYAxisAnchor? = nil,
                left: NSLayoutXAxisAnchor? = nil,
                bottom: NSLayoutYAxisAnchor? = nil,
                right: NSLayoutXAxisAnchor? = nil,
                paddingTop: CGFloat = 0,
                paddingLeft: CGFloat = 0,
                paddingBottom: CGFloat = 0,
                paddingRight: CGFloat = 0,
                width: CGFloat = 0,
                height: CGFloat = 0
        ) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    public func anchor(top: NSLayoutYAxisAnchor? = nil,
                       left: NSLayoutXAxisAnchor? = nil,
                       bottom: NSLayoutYAxisAnchor? = nil,
                       right: NSLayoutXAxisAnchor? = nil,
                       topConstant: CGFloat = 0,
                       leftConstant: CGFloat = 0,
                       bottomConstant: CGFloat = 0,
                       rightConstant: CGFloat = 0,
                       widthConstant: CGFloat = 0,
                       heightConstant: CGFloat = 0
        ) {
        translatesAutoresizingMaskIntoConstraints = false
        
        _ = anchorWithReturnAnchors(top, left: left, bottom: bottom, right: right, topConstant: topConstant, leftConstant: leftConstant, bottomConstant: bottomConstant, rightConstant: rightConstant, widthConstant: widthConstant, heightConstant: heightConstant)
    }
    
    public func anchorWithReturnAnchors(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    func fillSuperview(padding: UIEdgeInsets = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superviewTopAnchor = superview?.topAnchor {
            topAnchor.constraint(equalTo: superviewTopAnchor, constant: padding.top).isActive = true
        }
        
        if let superviewBottomAnchor = superview?.bottomAnchor {
            bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -padding.bottom).isActive = true
        }
        
        if let superviewLeadingAnchor = superview?.leadingAnchor {
            leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
        }
        
        if let superviewTrailingAnchor = superview?.trailingAnchor {
            trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
        }
    }
    
    func centerInSuperview(size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superviewCenterXAnchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: superviewCenterXAnchor).isActive = true
        }
        
        if let superviewCenterYAnchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: superviewCenterYAnchor).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
    
    public func anchorCenterXToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    public func anchorCenterYToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    
    public func anchorCenterSuperview() {
        anchorCenterXToSuperview()
        anchorCenterYToSuperview()
    }
    
    func overlayMiniAlertView(text: String, width: CGFloat?, height: CGFloat?, xOffset: CGFloat? = 0, yOffset: CGFloat? = 0, timeDisplayed: TimeInterval?, completion: ((Bool) -> ())? = nil) {
        
        removeFromSuperview(viewsToRemove: MiniAlertView.self)
        
        let miniAlertView: MiniAlertView = {
            let mav = MiniAlertView(text: text)
            mav.frame = CGRect.zero
            mav.translatesAutoresizingMaskIntoConstraints = false
            return mav
        }()
        
        self.addSubview(miniAlertView)
        
        miniAlertView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: xOffset ?? 0).isActive = true
        miniAlertView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: yOffset ?? 0).isActive = true
        miniAlertView.widthAnchor.constraint(equalToConstant: width ?? 120).isActive = true
        miniAlertView.heightAnchor.constraint(equalToConstant: height ?? 45).isActive = true
        
        UIView.animate(withDuration: 0.5, animations: {
            miniAlertView.scale(values: [1, 1.15, 1], duration: 0.5, repeatsForever: false)
            miniAlertView.alpha = 1
        }, completion: { (completed) in
            UIView.animate(withDuration: 0.5, delay: timeDisplayed ?? 2, options: UIView.AnimationOptions.curveEaseIn, animations: {
                miniAlertView.alpha = 0
            }, completion: { (completed) in
                completion?(true)
                miniAlertView.removeFromSuperview()
                
            })
        })
    }
    
    enum ScreenEdge {
        case top, bottom
    }
    
    func animateMiniAlertViewIn(from: ScreenEdge, text: String, relativeWidthMultiplier: CGFloat, height: CGFloat = 44, timeDisplayed: TimeInterval?) {
        
        removeFromSuperview(viewsToRemove: MiniAlertView.self)
        
        let miniAlertView: MiniAlertView = {
            let mav = MiniAlertView(text: text)
            mav.frame = CGRect.zero
            mav.backgroundColor = .systemBackground
            mav.label.textColor = .label
            mav.label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
            mav.translatesAutoresizingMaskIntoConstraints = false
            mav.layer.applySketchShadow(color: .black, alpha: 0.15, x: 0, y: 0, blur: 15, spread: 0)
            mav.layer.cornerRadius = height / 2
            mav.alpha = 0
            return mav
        }()
        
        self.addSubview(miniAlertView)
        
        miniAlertView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        miniAlertView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: relativeWidthMultiplier).isActive = true
        miniAlertView.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        var offset: CGFloat = 0
        
        switch from {
        case .top:
            offset = (20)
            miniAlertView.topAnchor.constraint(equalTo: self.parentViewController?.safeTopAnchor ?? topAnchor, constant: offset).isActive = true
        case .bottom:
            offset = -(20)
            miniAlertView.bottomAnchor.constraint(equalTo: self.parentViewController?.safeBottomAnchor ?? bottomAnchor, constant: offset).isActive = true
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            miniAlertView.frame.origin.y += offset
            
            miniAlertView.alpha = 1
        }, completion: { (completed) in
            miniAlertView.scale(values: [1, 1.02, 1], duration: 0.3, repeatsForever: false)
            UIView.animate(withDuration: 0.2, delay: timeDisplayed ?? 2, options: UIView.AnimationOptions.curveEaseIn, animations: {
                miniAlertView.frame.origin.y -= offset
            }, completion: { (completed) in
                miniAlertView.removeFromSuperview()
            })
        })
        
    }
    
    
    
    ///Cleans any view of the specified type from the superview
    func removeFromSuperview<T>(viewsToRemove: T.Type) {
        for subview in self.subviews {
            if subview is T {
                subview.removeFromSuperview()
                print("Removed a view of type \(T.self)")
                return
            }
        }
        print("View of type \(viewsToRemove) not found")
    }
}

extension UITextView: UITextViewDelegate {
    
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLbl = self.viewWithTag(50) as? UILabel {
                placeholderText = placeholderLbl.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
                placeholderLbl.text = newValue
                placeholderLbl.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    public var placeholderTextColor: UIColor? {
        get {
            if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
                return placeholderLbl.textColor
            } else {
                return nil
            }
        }
        set {
            if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
                placeholderLbl.textColor = newValue
            }
        }
    }
    
    public var placeholderFont: UIFont? {
        get {
            if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
                if let font = placeholderLbl.font {
                    return font
                }
                return nil
            } else {
                return nil
            }
        }
        set {
            if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
                placeholderLbl.font = newValue
            }
        }
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLbl = self.viewWithTag(50) as? UILabel {
            placeholderLbl.isHidden = self.text.count > 0
        }
    }
    
    private func resizePlaceholder() {
        if let placeholderLbl = self.viewWithTag(50) as! UILabel? {
            let x = self.textContainer.lineFragmentPadding
            let y = self.textContainerInset.top
            let width = self.frame.width - (x * 2)
            let height = placeholderLbl.frame.height
            
            placeholderLbl.frame = CGRect(x: x, y: y, width: width, height: height)
        }
    }
    
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLbl = UILabel()
        
        placeholderLbl.text = placeholderText
        placeholderLbl.sizeToFit()
        
        placeholderLbl.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        placeholderLbl.textColor = UIColor.lightGray
        placeholderLbl.tag = 50
        
        placeholderLbl.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLbl)
        self.resizePlaceholder()
        self.delegate = self
        self.tintColor = UIColor.white.withAlphaComponent(0.5)
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension Date {
    
    func fromString(string: String, format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: string)
    }
    
    func toString(dateFormat format: String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    ///Format: yyyy-MM-dd
    func today()-> String {
        let dayStr = day() < 10 ? "0\(day())" : "\(day())"
        return "\(yearYYYY())-\(monthMM())-\(dayStr)"
    }
    
    ///Returns current year as String
    ///2019 format
    func yearYYYY() -> Int {
        return DateFormatter.shared.calendar.component(.year, from: self)
    }
    
    func day() -> Int {
        return DateFormatter.shared.calendar.component(.day, from: self)
    }
    
    func month() -> Int {
        return DateFormatter.shared.calendar.component(.month, from: self)
    }
    
    ///Returns month as String containing two Ints »
    ///Jan = 01
    func monthMM()-> String {
        return month() < 10 ? "0\(month())" : "\(month())"
    }
    
    func timeAgo() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .abbreviated
        return formatter.localizedString(for: self, relativeTo: Date())
    }
}

extension DateFormatter {

    static let shared: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
}

extension UICollectionReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension Int {
    var formattedWithSeparator: String {
        return String(format: "%d", locale: Locale.current, self)
    }
}
extension UIButton {

    func applyDropShadow() {
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 5.0)
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.8
        
        layer.cornerRadius = 10
        clipsToBounds = true
        
        let rect = CGRect(x: bounds.width * 0.05, y: bounds.maxY - 10, width: bounds.width * 0.9, height: bounds.height * 0.03)
        layer.shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: layer.cornerRadius).cgPath
    }
}

extension Array {
    
    var middle: Element? {
        guard count != 0 else { return nil }
        
        let middleIndex = (count > 1 ? count - 1 : count) / 2
        return self[middleIndex]
    }
    
    var middleIndex: Int? {
        guard count != 0 else { return nil }
        return (count > 1 ? count - 1 : count) / 2
    }
    
    var firstQuartil: Int? {
        guard count != 0 else { return nil }
        return (count > 1 ? count - 1 : count) / 4
        
    }
    
    var thirdQuartil: Int? {
        guard count != 0 else { return nil }
        return (count > 1 ? count - 1 : count) / 8
        
    }
    
}


