//
//  BaseCrell.swift
//  Recipe Ideas
//
//  Created by az on 12/08/2019.
//  Copyright © 2019 az. All rights reserved.
//

import UIKit

//This is a default cell for collectionViews with behaviors that are built-in to reuse anywhere needed. isBounceble = true is an example of that. It allows for the cells to be slightly animated when highlighted. See ClipCell.swift to get an example implementation.

class BaseCell: UICollectionViewCell {
    
    var isBounceable: Bool = false
    
    var hasDropShadow: Bool = false {
        didSet {
            guard hasDropShadow else { return }
            layer.applySketchShadow(color: .black, alpha: 0.15, x: 0, y: 2, blur: 6, spread: 0)
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            guard isHighlighted != oldValue && isBounceable else {
                return
            }
            bounce(isHighlighted)
        }
    }
    
    func bounce(_ bounce: Bool) {
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1,
            options: [.allowUserInteraction, .beginFromCurrentState],
            animations: { self.transform = bounce ? CGAffineTransform(scaleX: 0.96, y: 0.96) : .identity },
            completion: nil)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

