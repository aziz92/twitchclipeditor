//
//  ColorLibrary.swift
//  StreamTracker
//
//  Created by az on 08/05/2020.
//  Copyright © 2020 az. All rights reserved.
//

import UIKit

enum ColorName: String {
    case twitchPurple
}

extension UIColor {
    
    static func main(_ name: ColorName) -> UIColor {
        guard UIColor(named: name.rawValue) != nil else {
            assertionFailure("💥💥💥  COLOR MISSING 💥💥💥")
            return .cyan
        }
        return UIColor(named: name.rawValue) ?? .cyan
    }
}
